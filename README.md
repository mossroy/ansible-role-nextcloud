# Ansible role Nextcloud

Simple Ansible role to install and configure Nextcloud in my self-hosting context

The `nextcloud_mysql_password` variable needs to be set.
The optional `nextcloud_legacy_root_password` variable can be set for MariaDB instances that were upgraded from Debian 9